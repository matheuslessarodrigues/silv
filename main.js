#!/usr/bin/env node
"use strict";

const commands = {
	util: [
		{ name: "quit", keys: "CTRL_C" },
		{ name: "help", keys: "CTRL_H" },
		{ name: "custom", keys: "CTRL_X", prompt: "custom command: " },
	],

	git: [
		{ name: "status", keys: "CTRL_S", commands: [{ run: "git -c color.status=always status" }] },
		{ name: "log", keys: "CTRL_L", commands: [{ run: "git log --all --decorate --oneline --graph -20 --color" }] },
		{ name: "branches", keys: "CTRL_B", commands: [{ run: "git branch -a" }] },
		{ name: "branch", keys: "B", commands: [{ run: "git branch %s", prompt: "branch name: " }] },
		{ name: "tag", keys: "CTRL_T", commands: [{ run: "git tag %s -f", prompt: "tag name: " }] },
		{ name: "commit", keys: "C", commands: [{ run: "git add --all ." }, { run: "git commit -m \"%s\"", prompt: "commit message: " }] },
		{ name: "update", keys: "CTRL_U", commands: [{ run: "git checkout \"%s\"", prompt: "checkout to: " }] },
		{ name: "merge", keys: "CTRL_M", commands: [{ run: "git merge \"%s\"", prompt: "merge with: " }] },
		{ name: "pull", keys: "CTRL_P", commands: [{ run: "git pull" }] },
		{ name: "push", keys: "P", commands: [{ run: "git push" }] },
		{ name: "revert", keys: "CTRL_R", commands: [{ run: "git reset --hard" }] },
	],

	hg: [
		{ name: "status", keys: "CTRL_S", commands: [{ run: "hg summary --color always" }, { run: "hg status --color always" }] },
		{ name: "log", keys: "CTRL_L", commands: [{ run: "hg log --graph --style compact -l 20 --color always" }] },
		{ name: "branches", keys: "CTRL_B", commands: [{ run: "hg branches" }] },
		{ name: "branch", keys: "B", commands: [{ run: "hg branch %s", prompt: "branch name: " }] },
		{ name: "tag", keys: "CTRL_T", commands: [{ run: "hg tag %s -f", prompt: "tag name: " }] },
		{ name: "commit", keys: "C", commands: [{ run: "hg commit --addremove -m \"%s\"", prompt: "commit message: " }] },
		{ name: "update", keys: "CTRL_U", commands: [{ run: "hg update \"%s\"", prompt: "update to: " }] },
		{ name: "merge", keys: "CTRL_M", commands: [{ run: "hg merge \"%s\"", prompt: "merge with: " }] },
		{ name: "pull", keys: "CTRL_P", commands: [{ run: "hg pull" }] },
		{ name: "push", keys: "P", commands: [{ run: "hg push" }] },
		{ name: "revert", keys: "CTRL_R", commands: [{ run: "hg revert --all" }] },
	]
}

const term = require('terminal-kit').terminal;
const clear = require('clear');
const util = require('util');
const exec = require('child_process').execSync;
const fs = require('fs');
const path = require('path');

let selected_commands = null;
let wait_for_input = false;

let custom_command_history = [];

const util_functions = {
	help: function (cmd) {
		console.log(commands);
	},
	quit: function (cmd) {
		clear_screen();
		term.processExit();
	},
	custom: function (cmd, defaultInput = "") {
		console.log("");
		term.yellow("custom command: ");
		wait_for_input = true;

		let options = {
			cancelable: true,
			default: defaultInput,
			history: custom_command_history,
		}

		term.inputField(options, function (e, text) {
			wait_for_input = false;
			if (text != null) {
				run(text);
				custom_command_history.push(text);

				print_done();
			} else {
				print_canceled();
			}
		});
	}
}

load_configs();
term.grabInput();

clear_screen();
term.bold.brightGreen("silv - git/hg client\n\n");

let help_command = find_by_name(commands.util, "help");
if (help_command != null) {
	console.log("use", help_command.keys, "for help");
}

if (fs.existsSync(".git")) {
	selected_commands = commands.git;
} else if (fs.existsSync(".hg")) {
	selected_commands = commands.hg;
}

if (selected_commands == null) {
	term.brightRed("no repository found");
	term.processExit();
	return;
}

term.on('key', function (key, matches, data) {
	if (wait_for_input == true) {
		return;
	}

	for (let i in matches) {
		let match = matches[i];

		let util_command = find_by_key(commands.util, match);
		if (util_command != null) {
			get_util_function(util_command)(util_command);
			return;
		}

		let command = find_by_key(selected_commands, match);
		if (command != null) {
			execute_command(command);
			return;
		}
	}

	if (is_letter(key)) {
		let util_command = find_by_name(commands.util, "custom");
		if (util_command != null) {
			get_util_function(util_command)(util_command, key);
		}
	}
});

function get_util_function(util_command) {
	clear_screen();
	term.bold().brightGreen(util.format("> %s\n", util_command.name));
	return util_functions[util_command.name];
}

function execute_command(command) {
	clear_screen();
	term.bold().brightGreen(util.format("> %s\n", command.name));

	let cmds = command.commands;

	for (let command of cmds) {
		if (command.prompt != null) {
			console.log("");
			term.yellow(command.prompt);
			wait_for_input = true;
			term.inputField({ cancelable: true }, function (e, text) {
				wait_for_input = false;
				if (text != null) {
					if (!run(util.format(command.run, text))) {
						return;
					}

					print_done();
				} else {
					print_canceled();
					return;
				}
			});
		} else {
			if (!run(command.run)) {
				return;
			}
		}
	}

	if (!wait_for_input) {
		print_done();
	}
}

function run(cmd) {
	if (cmd == null) {
		return false;
	}

	try {
		console.log("");
		term.brightGreen(cmd);
		console.log("\n");

		let out = exec(cmd);
		console.log(out.toString());

		return true;
	} catch (e) {
		console.log("");
		term.red(e.toString());
		console.log("");

		return false;
	}
}

function print_done() {
	term.brightGreen("\n\ndone\n");
}

function print_canceled() {
	term.brightRed("\n\ncanceled\n");
}

function find_by_key(commands, key) {
	for (let command of commands) {
		if (command.keys == key) {
			return command;
		}
	}
	return null;
}

function find_by_name(commands, name) {
	for (let command of commands) {
		if (command.name == name) {
			return command;
		}
	}
	return null;
}

function clear_screen() {
	clear();
}

function load_configs() {
	let configFileName = find_config_filename();
	if (configFileName == null) {
		return;
	}

	let config = JSON.parse(fs.readFileSync(configFileName, 'utf8'));

	for (let key in config) {
		let config_command_set = config[key];
		let command_set = commands[key];

		if (command_set == null) {
			commands[key] = config_command_set;
			continue;
		}

		for (let config_command of config_command_set) {
			remove_if_name_equals(command_set, config_command.name);
			command_set.push(config_command);
		}
	}
}

function find_config_filename() {
	let configFileName = path.join(__dirname, "config.json");
	if (fs.existsSync(configFileName)) {
		return configFileName;
	}

	return null;
}

function remove_if_name_equals(commands, name) {
	for (let i in commands) {
		let c = commands[i];
		if (c.name == name) {
			commands.splice(i, 1);
			break;
		}
	}
}

function is_letter(charVal) {
	return charVal.length == 1 && charVal.toUpperCase() != charVal.toLowerCase();
}